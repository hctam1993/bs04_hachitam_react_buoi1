import React, { Component } from "react";
import Layout_Footer from "./Layout_Footer";
import Layout_Header from "./Layout_Header";
import Layout_ListItem from "./Layout_ListItem";
import Layout_Nav from "./Layout_Nav";

export default class Layout extends Component {
  render() {
    return (
      <div>
        <Layout_Nav />
        <Layout_Header />
        <Layout_ListItem />
        <Layout_Footer />
      </div>
    );
  }
}
